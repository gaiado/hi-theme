<?php get_header();

if (have_posts()) : while (have_posts()) : the_post();
$images = get_field('gallery');
?>

        <a href="" class="btn btn-secondary btn-go ">Ver Portafolio </a>
        </div>
        </div>
        </div>
        </div>
        </section>


        <section>

            <div class="container">
                <div class="row">
                    <div class="col-md-6">

                    <?php foreach( $images as $image ): ?>
                            <div class="img-content pb-4 mb-4">
                                <img  src="<?php echo esc_url($image['url']); ?>">
                                
                            </div>

                            <?php endforeach;
                           $terms= get_the_terms( $post->ID, 'categoria');  
                           ?>

                    </div>
                    <div class="col-md-6">
                        <div class="img-content stick">
                            <p class="texto"><?php  the_field('fecha'); ?></p>
                            <h2 class=" text-wide p-b-2 my-2 text-left"><?php  foreach( $terms as $term ) {
                               
                            echo ' ', $term->name,' -'; } ?></h2>
                            <h3 class="title"><?php echo get_the_title(); ?></h3>
                            <p class="text"><?php echo the_content(); ?></p>

                            <a class="btn btn-circle btn-lg" href=""><i class="fab fa-facebook-f"></i></a>
                            <a class="btn btn-circle btn-lg" href=""><i class="fab fa-twitter"></i></a>
                        </div>

                    </div>
                </div>
            </div>

        </section>






<?php
    endwhile;
endif;
?>


<?php get_footer(); ?>