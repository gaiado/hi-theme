<footer class="navbar navbar-expand navbar-dark bg-dark">
    <div class="container">
        <div class="row align-items-center">  
            <div class="col-lg-4 col-md-5 text-right order-sm-3">
                <ul class="navbar-nav">
                    <?php include 'social-links.php'; ?>
                </ul>
            </div>                
            <div class="col-lg-4 col-md-2 text-center order-sm-2">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/logo.png" loading="lazy">
            </div>     
            <div class="col-lg-4 col-md-5 order-3 order-sm-1">
                <ul class="navbar-nav">
                    <li class="nav-item active">
                        <div class="nav-link">©2021 HECALE – INNOVANDO</div>
                    </li>
                </ul>
            </div>
        </div>        
    </div>
</footer>
</div>
</div>
<?php wp_footer(); ?>
<!-- JS, Popper.js, and jQuery -->
<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"></script>
<script src="https://unpkg.com/@barba/core"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/ScrollTrigger.min.js"></script>
<script src="<?php echo get_stylesheet_directory_uri(); ?>/dist/bundle.js?v=0.0.5"></script>
</body>

</html>