<?php get_header(); ?>
<p>
<strong>¿COMO LO HACEMOS? </strong>
</p>
<a href="#one" class="btn btn-secondary btn-go ">¡CONSULTA NUESTRO PLANES! </a>
</div>
</div>
</div>
</div>
</section>

<div>
</div>
<section id="one">
    <div class="text-center container">        
        
    <h2>NUESTROS PLANES</h2>
    <h3>¿CUÁL SE AJUSTA A TU PRESUPUESTO O TAMAÑO DE TU EMPRESA?</h3>
    </div>
    <div class="container mt-4">
        <div class="row text-center">

            <div class="col-sm-4 mt-4 mt-sm-0">
                <div class="card border border-secondary card-planes">
                    <h3 class="card-title p-1 mt-5 text-center"><i class="fa fa-chart-line" aria-hidden="true"></i><span> MEMORY PLAN</span></h3>
                    <div class="card-body">
                        <ul class="text-left list-group">
                            <li>
                                <p>Creación de cuentas de Facebook e Instagram</p>
                            </li>
                            <li>
                                <p>Optimización de Facebook, instagram, Messenger y Whatsapp Bussines</p>
                            </li>
                            <li>
                                <p>Reporte mensual y junta con el cliente</p>
                            </li>
                            <li>
                                <p>Diseño y edición de gráficos, imagenes y video.</p>
                            </li>
                            <li>
                                <p>Segmentación digital de mercardo</p>
                            </li>
                            <li>
                                <p>Calendario de publicaciones</p>
                            </li>
                            <li>
                                <p>Redacción y programación de contenido</p>
                            </li>
                            <li>
                                <p>Guía de manejo de redes sociales y asesoría</p>
                            </li>
                            <li>
                                <p>Monitoreo y atención a la comunidad</p>
                            </li>
                            <li>
                                <p>16 publicaciones al mes</p>
                            </li>
                            <li>
                                <p>2 visitas al mes para sesión de fotos</p>
                            </li>
                            <li>
                                <p>1 video institucional</p>
                            </li>
                            <li>
                                <p>1 video persuasivo al mes</p>
                            </li>
                        </ul>
                        <a href="<?php get_site_url(); ?>/contacto" class="btn btn-secondary btn-go">CONTACTAR</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mt-4 mt-sm-0">
                <div class="card border border-secondary card-planes">
                    <h3 class="card-title p-0 mt-5 text-center"><i class="fa fa-crown" aria-hidden="true"></i><span> MATURITY PLAN</span></h3>
                    <div class="card-body">
                        <ul class="text-left list-group">
                            <li>
                                <p>Creación de cuentas de Facebook e Instagram</p>
                            </li>
                            <li>
                                <p>Optimización de Facebook, instagram, Messenger y Whatsapp Bussines</p>
                            </li>
                            <li>
                                <p>Reporte mensual y junta con el cliente</p>
                            </li>
                            <li>
                                <p>Diseño y edición de gráficos, imagenes y video.</p>
                            </li>
                            <li>
                                <p>Segmentación digital de mercardo</p>
                            </li>
                            <li>
                                <p>Calendario de publicaciones</p>
                            </li>
                            <li>
                                <p>Redacción y programación de contenido</p>
                            </li>
                            <li>
                                <p>Guía de manejo de redes sociales y asesoría</p>
                            </li>
                            <li>
                                <p>Monitoreo y atención a la comunidad</p>
                            </li>
                            <li>
                                <p>Desarrollo de promocionales de productos y servicios</p>
                            </li>
                            <li>
                                <p>Cotizaciones para soluciones publicitarias alternas y/o complementarias</p>
                            </li>
                            <li>
                                <p>24 publicaciones al mes</p>
                            </li>
                            <li>
                                <p>4 visitas al mes para sesión de fotos</p>
                            </li>
                            <li>
                                <p>Cubrir hasta 2 eventos al mes</p>
                            </li>
                            <li>
                                <p>1 video institucional</p>
                            </li>
                            <li>
                                <p>1 video persuasivo al mes</p>
                            </li>


                        </ul>
                        <a href="<?php get_site_url(); ?>/contacto" class="btn btn-secondary btn-go">CONTACTAR</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 mt-4 mt-sm-0">
                <div class="card bg-primary card-planes">
                    <h3 class="card-title p-0 mt-5"><i class="fa fa-gem" aria-hidden="true"></i><span> LOYALTY PLAN</span></h3>
                    <div class="card-body">
                        <ul class="text-left text-light list-group">
                            <li>
                                <p>Creación de cuentas de Facebook e Instagram</p>
                            </li>
                            <li>
                                <p>Optimización de Facebook, instagram, Messenger y Whatsapp Bussines</p>
                            </li>
                            <li>
                                <p>Reporte mensual y junta con el cliente</p>
                            </li>
                            <li>
                                <p>Diseño y edición de gráficos, imagenes y video.</p>
                            </li>
                            <li>
                                <p>Segmentación digital de mercardo</p>
                            </li>
                            <li>
                                <p>Calendario de publicaciones</p>
                            </li>
                            <li>
                                <p>Redacción y programación de contenido</p>
                            </li>
                            <li>
                                <p>Guía de manejo de redes sociales y asesoría</p>
                            </li>
                            <li>
                                <p>Monitoreo y atención a la comunidad</p>
                            </li>
                            <li>
                                <p>Desarrollo de promocionales de productos y servicios</p>
                            </li>
                            <li>
                                <p>Cotizaciones para soluciones publicitarias alternas y/o complementarias</p>
                            </li>
                            <li>
                                <p>30 publicaciones al mes</p>
                            </li>
                            <li>
                                <p>4 visitas al mes para sesión de fotos</p>
                            </li>
                            <li>
                                <p>Cubrir hasta 2 eventos al mes</p>
                            </li>
                            <li>
                                <p>1 video institucional</p>
                            </li>
                            <li>
                                <p>2 videos persuasivos al mes</p>
                            </li>
                            <li>
                                <p>Vuelo de Dron</p>
                            </li>
                            <li>
                                <p>Publicidad sugeridad para redes sociales</p>
                            </li>

                        </ul>
                        <a href="<?php get_site_url(); ?>/contacto" class="btn btn-secondary btn-go">CONTACTAR</a>
                    </div>
                </div>
            </div>

        </div>
        <div class="text-center mt-4">
            <p>
            <strong>¡CONTACTANOS Y AGENDA UNA REUNION PARA ASESORARTE!</strong>
            </p>
        </div>
    </div>

</section>

<section id="two">
    <h3 class="text-center">OTROS SERVICIOS</h3>
    <div class="container mt-4">
        <div class="row">
    <div class="col-sm-6">
            <a class="d-block mb-4">
                <div class="card carta  shadow">
                    <div class="card-body carta2 ">
                        <div class="row mt-2">
                            <div class="col-lg-2 pl-lg-2 text-center"><img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/planes/camera.svg');" alt=""></div>
                            <div class="col-lg-10 pt-2 ">
                                <h4 class="subtitulo">Fotografía y video</h4>
                                <p>¿Necesitas cubrir un evento especial de tu negocio? Podemos ayudarte a capturar los mejores momentos a través de foto y video. </p>
                            </div>
                        </div>
                    </div>
                </div>
</a>
            <a class="d-block mb-4">
                <div class="card carta  shadow">
                    <div class="card-body carta2">
                        <div class="row mt-2">
                            <div class="col-lg-2 pl-lg-2 text-center"><img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/planes/drone.svg');" alt=""></div>
                            <div class="col-lg-10 pt-2 ">
                                <h4 class="subtitulo">Vuelos de drone</h4>
                                <p>¿Requieres hacer una sesión muy especial? Podemos ayudarte con las mejores tomas aéreas y con muy buena calidad. </p>
                            </div>
                        </div>
                    </div>
                </div>
            </a>
            </div>
          <div class="col-sm-6">
            <a class="d-block mb-4">
                <div class="card carta shadow">
                    <div class="card-body carta2">
                        <div class="row mt-2">
                        <div class="col-lg-2 pl-lg-2 text-center"><img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/planes/pc.svg');" alt=""></div>
                            <div class="col-lg-10 pt-2 ">
                                <h4 class="subtitulo">Marketing de contenidos</h4>
                                <p>Desarrollamos la estrategia de contenido, te realizamos el diseño y te asesoramos para que la implementes en tus redes sociales. </p>
                            </div>
                        </div>
                    </div>
                </div>
</a>
            <a class="d-block mb-4">
                <div class="card carta shadow">
                    <div class="card-body carta2">
                        <div class="row mt-2">
                            <div class="col-lg-2 pl-lg-2 text-center"><img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/planes/guy.svg');" alt=""></div>
                            <div class="col-lg-10 pt-2 ">
                                <h4 class="subtitulo">Manual de identidad y registro de marca</h4>
                                <p>¡No solo es un gráfico más! Dale vida y forma a tu marca, establecemos todos los parámetros para su implementación y la registramos formalmente a tu nombre o al de tu negocio. </p>
                            </div>
                        </div>
                    </div>
                </div>
</a>
</div>
        </div>
        <div class="text-center mt-4">
        <p>
<a href="<?php get_site_url(); ?>/contacto" class="btn btn-secondary btn-go">CONTÁCTANOS</a>
</p>
            <strong>¡AGENDA UNA REUNION PARA ASESORARTE!</strong>
            
        </div>
    </div>


</section>

<?php get_footer(); ?>