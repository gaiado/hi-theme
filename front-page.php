<?php get_header(); ?>

<a href="<?php get_site_url(); ?>/contacto" class="btn btn-secondary btn-go">CONTÁCTANOS</a>

</div>
</div>
</div>
</div>
</section>

<link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
<div>
    <!--INICIO SECCIÓN -->
    <section id="first" class="container">
        <div class="row align-items-center">
            <div class="col-sm-6 col-xs-12">
                <h2>ACERCA DE NOSOTROS</h2>
                <h3>¿POR QUÉ<span> HECALE – INNOVANDO </span>ES SU MEJOR ALIADO?</h3>
                <p>Porque más allá de entregarte un servicio profesional buscamos un sentido de identidad con tu negocio, que nos haga sentir parte del mismo para darte soluciones ante cualquier problemática u oportunidad y buscamos tu mejora continua.</p>
                <p>Somos un equipo de profesionales que creció y vive en Tulum, entendemos cómo funciona el mercado y hacia donde se dirige el desarrollo, todas nuestras relaciones son estrechas. </p>
                <div class="d-block d-sm-none">
                <div class="elipse">
                    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/first.png" alt="First-image">
                </div>
                </div>
                <a href="<?php get_site_url(); ?>/contacto" class="btn btn-secondary btn-go">CONTÁCTANOS</a>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="d-none d-sm-block"> 
                <div class="elipse">
                    <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/first.png" alt="First-image">
                </div>
                </div>
            </div>

        </div>

    </section>
    <!--SALTO DE SECCIÓN -->
    <!--INICIO SECCIÓN -->
    <section id="second">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-sm-5 col-xs-12">
                    <div class="d-none d-sm-block">
                    <div class="grid">
                        <div class="img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/third.png" alt="">
                        </div>
                        <div class="img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/third.png" alt="">
                        </div>
                        <div class="img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/third.png" alt="">
                        </div>
                    </div>
                    </div>
                </div>
                <div class="col-sm-7 col-xs-12">
                    <h2>NUESTROS SERVICIOS</h2>
                    <h3>CONTAMOS CON SERVICIOS CLAVES PARA HACER CRECER TU NEGOCIO</h3>
                    <p>Contamos con servicios claves para hacer crecer tu negocio, mantenerlo ordenado y proponerte nuevas oportunidades de inversión. </p>
                    <div class="minis">
                        <div class="row">
                            <div class="col-6">
                                <div class="form-check">
                                    
                                    <label class="form-check-label subtitulo"> Branding</label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-check">
                                    <label class="form-check-label subtitulo"> Tramitología </label>
                                </div>
                            </div>
                            <div class="col-6">
                                <div class="form-check">
                                    <label class="form-check-label subtitulo"> Bienes Raíces </label>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="d-block d-sm-none">
                    <div class="grid ">
                        <div class="img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/third.png" alt="">
                        </div>
                        <div class="img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/third.png" alt="">
                        </div>
                        <div class="img">
                            <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/third.png" alt="">
                        </div>
                    </div>
                    </div>
                    <div class="text-right text-sm-left mt-4">
                    <a href="<?php get_site_url(); ?>/branding" class="btn btn-secondary">VER TODOS LOS SERVICIOS</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--SALTO DE SECCIÓN -->
<?php if(0){ ?>
    <!--INICIO SECCIÓN -->
    <section class="third">
        <h2>ULTIMOS PROYECTOS</h2>
        <div class="text-center container">
            <div class="row justify-content-center">
                <div class="col-xl-6 col-lg-7">
                    <h3>CLIENTES QUE CONFIARON EN NOSOTROS</h3>
                </div>
            </div>
        </div>


        <div class="container">

            <div class="row">
                <?php for($i = 0; $i < 6; $i++) { ?>
                <div class="col-lg-4 col-sm-6">
                    <div class="card">
                        <div class="card-img-top">
                            <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/third.png" alt="Carousel 2">
                        </div>
                        <div class="card-block row align-items-center">
                            <div class="col-9">
                                <p class="small  text-wide p-b-2 my-2 ml-3 text-left">Marketing - Fotografía - Social Media</p>
                                <h5 class="subtitulo my-1 pb-2 ml-3 text-left">Los Amigos de Tulum</h5>
                            </div>
                            <div class="col-3">
                                <a href="#" class="btn-cat"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="btn-ver-proyectos">
            <a href="<?php get_site_url(); ?>/portafolio" class="btn btn-secondary">VER TODOS LOS PROYECTOS</a>
        </div>


    </section>
    <!--SALTO DE SECCIÓN -->
    <!--INICIO SECCIÓN -->
    <section id="fourth">

        <h2>OPINIONES DE CLIENTES</h2>
        <h3>AGRADECEMOS SU CONFIANZA</h3>


        <div class="container pt-4">
            <div class="row">
                <div class="swiper-container">
                    <div class="swiper-wrapper">

                        <div class="swiper-slide col-md-4">
                            <div class="card text-center">
                                <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/user-4.png" alt="">
                                <div class="card-body">
                                    <h5 class="card-title">Ruben Alejandro</h5>
                                    <p class="subtitulo">Gran Planaeromexico</p>
                                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi vero, eum debitis totam voluptatibus mollitia</p>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide col-md-4 ">
                            <div class="card text-center">
                                <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/user-4.png" alt="">
                                <div class="card-body">
                                    <h5 class="card-title">Ruben Alejandro</h5>
                                    <p class="subtitulo">Gran Planaeromexico</p>
                                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi vero, eum debitis totam voluptatibus mollitia</p>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide col-md-4">
                            <div class="card text-center">
                                <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/user-4.png" alt="">
                                <div class="card-body">
                                    <h5 class="card-title">Ruben Alejandro</h5>
                                    <p class="subtitulo">Gran Planaeromexico</p>
                                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi vero, eum debitis totam voluptatibus mollitia</p>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide col-md-4">
                            <div class="card text-center">
                                <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/user-4.png" alt="">
                                <div class="card-body">
                                    <h5 class="card-title">Ruben Alejandro</h5>
                                    <p class="subtitulo">Gran Planaeromexico</p>
                                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi vero, eum debitis totam voluptatibus mollitia</p>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide col-md-4 ">
                            <div class="card text-center">
                                <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/user-4.png" alt="">
                                <div class="card-body">
                                    <h5 class="card-title">Ruben Alejandro</h5>
                                    <p class="subtitulo">Gran Planaeromexico</p>
                                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi vero, eum debitis totam voluptatibus mollitia</p>
                                </div>
                            </div>
                        </div>

                        <div class="swiper-slide col-md-4 ">
                            <div class="card text-center">
                                <img class="img-fluid" src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/user-4.png" alt="">
                                <div class="card-body">
                                    <h5 class="card-title">Ruben Alejandro</h5>
                                    <p class="subtitulo">Gran Planaeromexico</p>
                                    <p>Lorem, ipsum dolor sit amet consectetur adipisicing elit. Eligendi vero, eum debitis totam voluptatibus mollitia</p>
                                </div>
                            </div>
                        </div>


                    </div>
                    <!-- Add Pagination -->
                    <div class="swiper-pagination"></div>
                    <!-- Add Arrows -->
                    <!--  <div class="swiper-button-next"></div>
                    <div class="swiper-button-prev"></div>
-->
                </div>

            </div>
        </div>
        <!-- Swiper -->


        <script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>


    </section>
    <!--SALTO DE SECCIÓN -->
<?php } ?>
    <!--INICIO SECCIÓN -->
    <section id="fifth" style="background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/src/img/background-5.png');">
        <div class="container">
            <div class="row ">
                <div class="col-sm-6">
                    <h2>CONTACTO</h2>
                    <h3>¿LISTO PARA DAR EL SIGUIENTE PASO?</h3>
                    <a href="<?php get_site_url(); ?>/contacto" class="btn btn-secondary btn-go"> SI, QUIERO SABER MÁS </a>
                </div>

            </div>
        </div>
    </section>
</div>
<?php get_footer(); ?>