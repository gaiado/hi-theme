<?php get_header(); ?>

<a href="" class="btn btn-secondary btn-go ">Ver Portafolio </a>
</div>
</div>
</div>
</div>
</section>



<section class="third">

    <div class="container">

        <div class="row">
            <?php
            $query = new WP_Query(array(
                'post_type' => 'proyecto'
            ));
            while ($query->have_posts()) {
                $query->the_post();


                // for ($i = 0; $i < 6; $i++) { 
            ?>
                <div class="col-lg-4 col-sm-6">
                    <div class="card">
                        <div class="card-img-top">
                            <img class="img-fluid" src="<?php echo get_the_post_thumbnail_url(); ?>" alt="Carousel 2">
                        </div>
                        <div class="card-block row align-items-center">
                            <div class="col-9">
                                <p class="small text-wide p-b-2 my-2 ml-3 text-left">Marketing - Fotografía - Social Media</p>
                                <h5 class="subtitulo my-1 pb-2 ml-3 text-left"><?php echo get_the_title(); ?></h5>
                            </div>
                            <div class="col-3">
                                <a href="<?php echo get_the_permalink(); ?>" class="btn-cat"><i class="fa fa-arrow-right" aria-hidden="true"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            <?php  } ?>
        </div>
    </div>
</section>

<?php get_footer(); ?>