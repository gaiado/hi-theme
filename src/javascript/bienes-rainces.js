export function initBienesRainces() {
    /*const updateValues = function() {
        let inputValue = document.querySelector('#myRange').value;
        document.querySelector('#output1').innerHTML = inputValue;
    }
    const updateValues2 = function() {
        let inputValue2 = document.querySelector('#myRange2').value;
        document.querySelector('#output2').innerHTML = inputValue2;
    }
    document.querySelector('#myRange').oninput = updateValues;
    document.querySelector('#myRange2').oninput = updateValues2;*/

    $(function () {
        $("#slider-range").slider({
            range: true,
            min: 200,
            max: 1000000,
            values: [200, 1000000],
            slide: function (event, ui) {
                $("#amount").val(ui.values[0] + " - " + ui.values[1]);
            }
        });
        $("#amount").val($("#slider-range").slider("values", 0) +
            " - " + $("#slider-range").slider("values", 1));
    });
    
    $(function () {
        $("#slider-rangeV").slider({
            range: true,
            min: 200,
            max: 1000000,
            values: [200, 1000000],
            slide: function (event, ui) {
                $("#amountV").val(ui.values[0] + " - " + ui.values[1]);
            }
        });
        $("#amountV").val($("#slider-rangeV").slider("values", 0) +
            " - " + $("#slider-rangeV").slider("values", 1));
    });
};
