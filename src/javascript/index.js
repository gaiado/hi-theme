import '../sass/styles.scss';




import { initFrontPage } from './front-page';
import { initBienesRainces } from './bienes-rainces';

var mainInit = function () {
  //si se necesita recargar algo en cada pagina
};

const loadingScreen = document.querySelectorAll('ul.transition li')

barba.init({
  transitions: [{
    leave(data) {
      return gsap.to(loadingScreen, { duration: .25, scaleY: 1, transformOrigin: 'bottom left', stagger: .15 })
    },
    after(data) {
      return gsap.to(loadingScreen, { duration: .25, scaleY: 0, transformOrigin: 'top left', stagger: .15 })
    }
  }],
  views: [{
    namespace: 'home',
    afterEnter(data) {
      setTimeout(initFrontPage, 1000);
    }
  }, {
    namespace: 'page-contacto',
    afterEnter(data) {
      setTimeout(initContacto, 1000);
    }
  }, {
    namespace: 'page-bienes-raices',
    afterEnter(data) {
      setTimeout(initBienesRainces, 1000);
    }
  }]
});

barba.hooks.enter(() => {
  window.scrollTo(0, 0);
});

barba.hooks.after(() => {
  mainInit();
});

$(function () {
  mainInit();
  $(window).scroll(function () {
    $('body').toggleClass('scrolled', $(this).scrollTop() > document.querySelector('nav').clientHeight + 70);
  }).scroll();
});
