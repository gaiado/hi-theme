<?php get_header(); ?>

<!--a href="" class="btn btn-secondary btn-go">VER OPCIONES</a-->

</div>
</div>
</div>
</div>
</section>

<section>
    <div class="container tabs2">
        <ul class="nav nav-tabs " id="myTab" role="tablist">
            <li class="nav-item col-6 tri-1">
                <a class="nav-link active " id="home-tab" data-toggle="tab" href="#one" role="tab" aria-controls="home" aria-selected="true">COMPRAR</a>
            </li>
            <li class="nav-item col-6 tri-2">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#two" role="tab" aria-controls="profile" aria-selected="false">VENDER</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="one" role="tabpanel" aria-labelledby="home-tab">
                <div class="">
                    <div class="texto">
                        <p>Te enviaremos las mejores opciones de propiedades, ayúdanos a saber en específico en que estas interesado. </p>
                    </div>

                    <div>
                    <?php echo do_shortcode('[contact-form-7 id="128" title="Compra"]'); ?>
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="two" role="tabpanel" aria-labelledby="profile-tab">
                <div class="">
                    <div class="texto">
                        <p>Obtén un precio justo por tu propiedad, ayúdanos a saber en específico más sobre ella.  </p>
                    </div>
                    <div >
                    <?php echo do_shortcode('[contact-form-7 id="139" title="Venta"]'); ?>
                    </div>

                </div>
            </div>


        </div>
    </div>


</section>

<?php get_footer(); ?>