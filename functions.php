<?php
require_once get_template_directory() .
  '/libs/class-wp-bootstrap-navwalker.php';

function brotherhoodcustom_post()
{
  register_post_type('proyecto', [
    'labels' => [
      'name' => __('Proyectos'),
      'singular_name' => __('Proyecto'),
    ],
    'public' => true,
    'has_archive' => true,
    'show_in_rest' => true,
    'rewrite' => ['slug' => 'portafolio'],
    'supports' => ['title', 'editor', 'thumbnail', 'excerpt'],
    'menu_icon' => 'dashicons-id-alt',
  ]);

  register_taxonomy(
    'categoria',
    ['proyecto'],
    [
      'hierarchical' => true,
      'labels' => [
        'name' => __('Categorias'),
        'singular_name' => __('Categoria'),
      ],
      'show_in_rest' => true,
      'show_ui' => true,
    ]
  );
}

function my_acf_json_save_point( $path ) 
{
  return get_stylesheet_directory() . '/acf-json';
}

function brotherhoodsetup()
{
  add_theme_support('post-thumbnails');
  //add_image_size('member-thumbnail', 360, 360, true);
}

function brotherhoodstyles()
{
  wp_enqueue_style(
    'style',
    get_template_directory_uri() . '/dist/bundle.css',
    [],     
    '0.0.5'                 
  );  
  wp_enqueue_style(
    'font-awesome',
    '//cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/css/all.min.css'
  ); 
  wp_enqueue_style(
    'jquery-ui',
    'https://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css'
  ); 

  /*wp_enqueue_style(
    'swiper',
    'https://unpkg.com/swiper/swiper-bundle.min.css'
  );*/
}

function brotherhoodmenus()
{
  register_nav_menus([
    'header-menu' => __('Header Menu', 'brotherhood')
  ]);
}

add_filter('acf/settings/save_json', 'my_acf_json_save_point');

//Hooks
add_action('after_setup_theme', 'brotherhoodsetup');
add_action('after_setup_theme', 'brotherhoodmenus');
add_action('wp_enqueue_scripts', 'brotherhoodstyles');
add_action('init', 'brotherhoodcustom_post');
