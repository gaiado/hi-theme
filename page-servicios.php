<?php get_header(); ?>

<a href="#second" class="btn btn-secondary btn-go">VER SERVICIOS</a>

</div>
</div>
</div>
</div>
</section>


<section id="second">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-5 ">
                <div class="grid">
                    <div class="img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/servicios/1-1.png" alt="">
                    </div>
                    <div class="img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/servicios/1-2.png" alt="">
                    </div>
                    <div class="img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/servicios/1-3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-sm-7 rej-1">
                <h2>TRAMITES MUNICIPALES</h2>
                <p>La tramitología además de ser una ciencia para resolver y facilitar los tramites también es considerada un arte, evítate el tiempo de espera, estar regresando una y otra vez a cada dependencia, hacer el papeleo y de buscar quien te ofrece el mejor precio de los servicios complementarios para tu tramite. ¡tenemos relaciones estrechas y a los mejores prestadores de servicios complementarios!/p>
                <div class="tabs1">
                    <ul class="nav nav-tabs " id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#one" role="tab" aria-controls="home" aria-selected="true">TESORERÍA</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#two" role="tab" aria-controls="profile" aria-selected="false">DESARROLLO URBANO</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#three" role="tab" aria-controls="contact" aria-selected="false">PROTECCIÓN CIVIL</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#four" role="tab" aria-controls="contact" aria-selected="false">SALUD</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="one" role="tabpanel" aria-labelledby="home-tab">
                            <div class="container">
                                <div class="row minis">
                                    <div class="col-6">
                                        <div class="form-check">
                                            
                                            <label class="form-check-label subtitulo"> Licencia Funcionamiento</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                            
                                            <label class="form-check-label subtitulo"> Predial</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                            
                                            <label class="form-check-label subtitulo"> Derecho de Recolección de Basura</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                            
                                            <label class="form-check-label subtitulo"> Certificado Deslinde Catastral</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                            
                                            <label class="form-check-label subtitulo"> Cedula Catastral</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="two" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="container">
                                <div class="row minis">
                                    <div class="col-6">
                                        <div class="form-check">
                                            
                                            <label class="form-check-label subtitulo"> Lic. Uso de Suelo GiroComercial</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                           
                                            <label class="form-check-label subtitulo"> Permiso Operación Ambiental</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                            
                                            <label class="form-check-label subtitulo"> Licencia Remoción Vegetal</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                            
                                            <label class="form-check-label subtitulo"> Licencia Obra Nueva</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                            
                                            <label class="form-check-label subtitulo"> Licencia de Regularización</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                           
                                            <label class="form-check-label subtitulo"> Licencia Modificacion</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                           
                                            <label class="form-check-label subtitulo"> Constancia de Zonificación Uso de Suelo</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                           
                                            <label class="form-check-label subtitulo"> Régimen de Propiedad en Condominio</label>
                                        </div>
                                    </div> 
                                    <div class="col-6">
                                        <div class="form-check">
                                           
                                            <label class="form-check-label subtitulo"> Lic. Terminación de Obra</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                           
                                            <label class="form-check-label subtitulo"> Lic. De Prorroga</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                           
                                            <label class="form-check-label subtitulo"> Lic. De Anuncio</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                           
                                            <label class="form-check-label subtitulo"> Lic. Ampliación / Remodelación</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                           
                                            <label class="form-check-label subtitulo"> Lic. Construcción Barda</label>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="three" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="container">
                                <div class="row minis">
                                    <div class="col-6">
                                        <div class="form-check">
                                            
                                            <label class="form-check-label subtitulo">  Anuencia Protección Civil</label>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <div class="form-check">
                                            
                                            <label class="form-check-label subtitulo"> Factibilidad de Riesgo</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="four" role="tabpanel" aria-labelledby="contact-tab">
                            <div class="container">
                                <div class="row minis">
                                    <div class="col-6">
                                        <div class="form-check">
                                           
                                            <label class="form-check-label subtitulo"> Anuencia Sanitaria</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section id="second">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-7 ">
                <h2>CONTRATACION DE SERVICIOS COMPLEMENTARIOS </h2>
                <h3>¡NO TE DEJAMOS SOLOS EN NINGUN MOMENTO!</h3>
                <p>Para realizar algún tramite es muy probable que necesites alguno de los siguientes servicios; quizá ya conozcas algunos de ellos, nosotros contamos con los mejores prestadores de servicios y así podemos ayudarte a tener completo y orden tu expediente. </p>
                <p>¡Podemos ayudarte!</p>
                <br>
                <h2>SERVICIOS, TRAMITES Y REQUISITOS COMPLEMENTARIOS</h2>
                <div class="minis container">
                    <div class="row">
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Recarga o Compra de Extintores</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                               
                                <label class="form-check-label subtitulo"> Aplicación Retardante Antiflama</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Plan Interno Protección Civil</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Dictamen Eléctrico</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Constancia de Fumigación</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Constancia de Sanitización</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Memoria de Calculo</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Estudio de Mecanica de Suelos</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                               
                                <label class="form-check-label subtitulo"> Resolutivo Impacto Ambiental</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                               
                                <label class="form-check-label subtitulo"> Proyecto Régimen de Condominio</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Estudios de Impacto Ambiental</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Autorización Impacto Ambiental</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                               
                                <label class="form-check-label subtitulo"> Plan de Manejo Residuos Solidos</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Estudios de Volúmenes y Calidad Agua</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Factibilidad CAPA</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Factibilidad CFE</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                               
                                <label class="form-check-label subtitulo"> Memoria Descriptiva del Proyecto</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                               
                                <label class="form-check-label subtitulo"> Reglamento de Condominio</label>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="form-check">
                                
                                <label class="form-check-label subtitulo"> Dictamen de Gas</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-5 rej-2">
                <div class="grid">
                    <div class="img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/servicios/2-1.png" alt="">
                    </div>
                    <div class="img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/servicios/2-2.png" alt="">
                    </div>
                    <div class="img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/servicios/3-3.png" alt="">
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>


<section id="second">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-sm-5 ">
                <div class="grid">
                    <div class="img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/servicios/3-1.png" alt="">
                    </div>
                    <div class="img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/servicios/3-2.png" alt="">
                    </div>
                    <div class="img">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/servicios/3-3.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-sm-7 rej-3">
                <h2>OTROS TRAMITES</h2>
                <h3>Atención y solución a notificaciones, citatorios, suspensiones y clausuras</h3>
                <p>¿Tienes incompleto tu tramite o te falta algún requisito para el funcionamiento ordenado de tu negocio? ¡podemos ayudarte! </p>
                <p>Cual sea el caso de tu negocio; conciliaremos y atenderemos las notificaciones, citatorios, suspensiones y clausuras, pero además de esto te ayudaremos a tener completos tus tramites y requisitos sin que nada te haga falta. </p>

            </div>
        </div>
    </div>
</section>





<?php get_footer(); ?>