<!DOCTYPE html>
<html lang="es_mx" ontouchmove>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title><?php
    bloginfo('name');
    wp_title();
    ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>" />
    <meta name="author" content="www.zonakb.com" />
    <?php wp_head(); ?>
</head>
<?php
    $namespace = '';
    $clase = '';
    $post = get_post(get_the_ID());
    $post_title = $post->post_title;
    $sub_title = get_field('subtitulo', $post->ID);
    $post_content = $post->post_content;
    $thumbnail = get_the_post_thumbnail_url( $post->ID);

    if (is_front_page()) {
      $namespace = 'home';
      $clase = $namespace;
    } elseif (is_archive()) {
      $namespace = 'archive-' . $post->post_type;
      $clase = 'archive ' . $post->post_type;
    } else {
      $namespace = $post->post_type . '-' . $post->post_name;
      $clase = $post->post_type . ' ' . $post->post_name;
    }
?>
<body data-barba="wrapper" onclick tabIndex=0>
    <ul class="transition">
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
    </ul>
    <div id="container" data-barba="container" data-barba-namespace="<?php echo $namespace; ?>">
        <div class="<?php echo $clase; ?>" id="<?php echo $post->ID; ?>">
            <nav class="navbar navbar-expand-lg navbar-dark fixed-top">
                <div class="container">
                    <a class="navbar-brand" href="<?php echo get_site_url(); ?>">
                        <img src="<?php echo get_stylesheet_directory_uri(); ?>/src/img/" height="30" class="d-inline-block align-top" alt="HECALE INNOVANDO" loading="lazy">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
        
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <?php
                            wp_nav_menu(array(
                                    'theme_location' => 'header-menu',
                                    'depth' => 2,
                                    'container' => false,
                                    'items_wrap' => '%3$s',
                                    'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
                                    'walker' => new WP_Bootstrap_Navwalker()
                                ));
                            ?>
                        </ul>
                        <ul class="navbar-nav ml-auto">                            
                            <?php include 'social-links.php'; ?>
                        </ul>
                    </div>
                </div>
            </nav>
            <section class="section-0"> 
                <div class="bg bg-image" style="background-image: url('<?php echo $thumbnail; ?>');">
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-8">
                            <h2>
                                <?php echo $post_title; ?>
                            </h2>
                            <h3>
                                <?php echo $sub_title; ?>
                            </h3>
                            <div>
                               <?php echo $post_content; ?>
                            </div>
                        <div>
                                